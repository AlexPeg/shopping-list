import React,{useState} from 'react';
import { StyleSheet,  View,TextInput,Button,TouchableWithoutFeedback} from 'react-native';

const AddProducts = ({submitHandler}) => {

    const [product,setProduct]=  useState('')
    const inputHandler = (val) => {
        setProduct(val)
       }

    const handleClick = () => {
      submitHandler(product);
      setProduct('')
    }

    return (
        <View style = {styles.inputContainer}>
            <TextInput
            style={ styles.textInput}
            placeholder="Nouveau produit"
            onChangeText={inputHandler}
            value= {product}
            
            />
            <Button
            title='Valider'
            onPress={handleClick}
            />
      </View>
    )
}

const styles= StyleSheet.create({
    textInput : {
        borderColor: "grey",
        borderWidth: 1,
        padding: 5,
        paddingLeft: 9,
        fontSize: 18,
        flexGrow:1
    
      },
      inputContainer: {
        flexDirection: "row",
        marginBottom: 15
      }
    
     
}


)


export default AddProducts;