import React, {useState} from 'react';
import { StyleSheet, Text, View,TextInput,Button,ScrollView,FlatList} from 'react-native';
import Products from './components/Products'
import AddProducts from './components/AddProducts';


export default function App() {

 
 const [myproducts,setMyProducts]=  useState([])

 

 const submitHandler = (product) => {

  if (product.length > 1) {
    const idString = Date.now().toString()
    setMyProducts (currentMyProducts => [{key: idString, name : product},...currentMyProducts,product] )
    
  }else {
    alert("Refusé")
  }
  
   
 }

 const deleteProduct = (key) => {
   setMyProducts(currentMyProducts => {
     return currentMyProducts.filter(product => product.key != key)   })
   
 }

  return (
    <View style={styles.container}>
      
        <AddProducts submitHandler= {submitHandler}/>
      <FlatList
        data={ myproducts }
        renderItem={({item}) => 
        <Products
         name= {item.name}
         idString={item.key}
         deleteProduct={deleteProduct}
         />}
      />
      {/* <ScrollView>
      <View style={styles.items}>
        {
          myproducts.map((product,index) => <Text style={styles.element} key= {index}>{product}</Text>)

          
          
        }

      </View>
      </ScrollView> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 40,
    paddingTop:60
  },
  inputContainer: {
    flexDirection: "row",
    marginBottom: 15
  },
  textInput : {
    borderColor: "grey",
    borderWidth: 1,
    padding: 5,
    paddingLeft: 9,
    fontSize: 18,
    flexGrow:1

  },

  items: {
    marginTop: 10
  },
  element: {
    backgroundColor:"#ffb6c1",
    padding:20,
    fontSize:17,
    marginVertical: 6
  }
});
